function roman(value) {
   const legend = "IVXLCDM";//index I=0,V=1,X=2,L=3,C=4,D=5,M=6
   const l=[1,5,10,50,100,500,1000];
   let sum=0;
   while(value){
      if(legend.indexOf(value[0]) < legend.indexOf(value[1])){ //เปรียบเทียบระหว่าง index ของข้อมูล ตำแหน่งแรกว่ามีค่า index น้อยกว่าตำแหน่งสองหรือไม่
         sum += (l[legend.indexOf(value[1])] - l[legend.indexOf(value[0])]);// sum ผลลัพธ์ โดยใช้ค่า value ใน array l เช่นข้อมูล IV = I(index=0,value=1) และ V(index=1,value=5) เพราะฉนั้น 5-1=4 จึง IV = 4
         value = value.substring(2, value.length);//กระโดดข้ามไป 2 จังหวะ
      } else {
         sum += l[legend.indexOf(value[0])];//ใช้ค่า ณ array ตำแหน่งที่ 0
         value = value.substring(1, value.length);//กระโดดข้ามไป 1 จังหวะ
      }
   }
   return sum;
}

// const a = roman('CLXXVIII');
// const a = roman('XXVIV');
// const a = roman('XIV');
// const a = roman('XC');
// const a = roman('CCXLIII');
// const a = roman('CCXLIV');
const a = roman('MMCMXCIX');

console.log(' Number : '+ a );

module.exports = {
    roman: roman,
};

